# BinoX #

This package provides an R interface to the BinoX tool for crosstalk
analysis. A workflow combining BinoX with clustering (MGclus or Markov
clustering) is also included, both as an R interface and a command line tool.
In addition, this package provides a framework for benchmarking the sensitivity
and specificity of several Gene Enrichment tools. An interface is provided to
easily plug-in any Gene Enrichment tool of interest and compare results with
existing tools. 26 gene expression datasets for which a target gene set is
known are included by default as testing data. More datasets can be added
using functions provided by this package.

### Dependecies ###

R packages:

AnnotationDbi, Biobase, doParallel, foreach, limma, methods,
readr, igraph, ggplot2, GEOquery, jsonlite, testthat, hgu133a.db, hgu133plus2.db,
argparse

Command line tools:

* [BinoX](http://sonnhammer.org/BinoX)
* [MGclus](http://sonnhammer.sbc.su.se/download/software/MGclus)
* [mcl](http://micans.org/mcl/index.html?sec_thesisetc)

See the reference manual for more instructions