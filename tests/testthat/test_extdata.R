context("Loading DataEntries from inst/extdata")

test_that("default DataEntries can be loaded from inst/extdata", {
  # Test that we can find the DataEntries directory shipped with the package.
  # (different from DataEntrie directory that end user will use)
  dataEntriesDir_package <- system.file("extdata", "DataEntries",
                                        package = "BinoX")
  expect_false(dataEntriesDir_package == "")
  expect_match(dataEntriesDir_package, "DataEntries")

  # Scrutinize a few examples
  experimentName1 <- "GSE1145"
  fileName1 <- paste0(experimentName1, ".json")
  de1 <- readJSONDataEntry(file.path(dataEntriesDir_package, fileName1),
                           showWarnings = TRUE)
  expect_identical(de1@GEO, "GSE1145")
  expect_identical(de1@targetDbName, "KEGG")
  expect_identical(de1@annotationDbName, "hgu133plus2.db")
  expect_identical(de1@source, character())
  expect_true(length(de1@samples) == 26)
  expect_true(length(de1@phenotypes) == 26)
  expect_identical(de1@block, factor())

  experimentName2 <- "GSE14924_CD4"
  fileName2 <- paste0(experimentName2, ".json")
  de2 <- readJSONDataEntry(file.path(dataEntriesDir_package, fileName2),
                           showWarnings = TRUE)
  expect_identical(de2@GEO, "GSE14924")

  experimentName3 <- "GSE16515"
  fileName3 <- paste0(experimentName3, ".json")
  de3 <- readJSONDataEntry(file.path(dataEntriesDir_package, fileName3),
                           showWarnings = TRUE)
  expect_identical(de3@GEO, "GSE16515")
  expect_true(length(de3@samples) == 30)
  expect_true(length(de3@phenotypes) == 30)
  expect_true(length(de3@block) == 30)

  # Check if all DataEntries can be loaded
  allFiles <- list.files(dataEntriesDir_package)
  for (fileName in allFiles) {
    de <- readJSONDataEntry(file.path(dataEntriesDir_package, fileName),
                            showWarnings = TRUE)
    expect_identical(class(de)[1], "DataEntry")
  }
})