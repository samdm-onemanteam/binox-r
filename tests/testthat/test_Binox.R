context("BinoX/Binox functions")
source(file.path('helpers', 'unitTestController.R'))

# ---------------------------------------------------------------------------- #
# Argument checks
# ---------------------------------------------------------------------------- #

test_that("the Binox R functions fail if Binox is not on $PATH", {
  # make $PATH empty
  sys_path <- Sys.getenv("PATH")
  Sys.setenv("PATH" = "")
  expect_error(Binox(),                  "Could not locate binary")
  expect_error(Binox_randomizeNetwork(), "Could not locate binary")
  expect_error(Binox_version(),          "Could not locate binary")
  # restore $PATH or we get problems in unrelated places
  Sys.setenv("PATH" = sys_path)
})

test_that("the Binox R functions can explain why the binary is not found", {
  testBinoxError <- function(fun) {
    errormessage = tryCatch(fun(cmdBinox = "IhopeThisCommandDoesNotExist"),
                            error = identity)
    expect_output(errormessage, "'Binox', command is")
    expect_output(errormessage, "IhopeThisCommandDoesNotExist")
    expect_output(errormessage, "PATH is")
  }
  testBinoxError(Binox)
  testBinoxError(Binox_randomizeNetwork)
  testBinoxError(Binox_version)
})

test_that("Binox_randomizeNetwork does sanity checks", {
  # at least one of networkPath or network must be given
  expect_error(Binox_randomizeNetwork(),
               "Arguments 'network' and 'networkPath' are missing")

  # an existing file may not be overwritten
  filepath <- tempfile(); file.create(filepath)
  expect_error(
    Binox_randomizeNetwork(networkPath = filepath, network = getAGraph()$ig),
    "already exists"
  )
  expect_error(
    Binox_randomizeNetwork(network = getAGraph()$ig, randNetPath = filepath),
    "already exists"
  )

  # argument randNetPath must be given
  expect_error(
    Binox_randomizeNetwork(network = getAGraph()$ig),
    '"randNetPath" is missing'
  )

  # network type check
  expect_error(
    Binox_randomizeNetwork(randNetPath = tempfile(), network = 42),
    "igraph or a data frame"
  )

})

test_that("Binox enrich does sanity checks", {
  # groups A have to given, somehow
  expect_error(
    Binox()
    , "Arguments 'groupsA', 'pathGroupsA' and 'pathClusterGroupsA' are missing"
  )
  expect_error(
    Binox(groupsA = getAGraph()$ga)
    , "No random network given"
  )

  # The existence of file randnetpath should be explicitly checked in R, if not
  # Binox will fail and the error message does not propagate well.
  expect_error(
    Binox(groupsA = getAGraph()$ga, randNetPath = "foo")
    , "Could not find 'randNetPath'"
  )

  # an existing file may not be overwritten
  filepathA <- tempfile(); file.create(filepathA)
  filepathB <- tempfile(); file.create(filepathB)
  filepathR <- tempfile(); file.create(filepathR)
  filepathC <- tempfile(); file.create(filepathC)
  filepathO <- tempfile(); file.create(filepathO)
  expect_error(
    Binox(groupsA = getAGraph()$ga, pathGroupsA = filepathA, randNetPath = filepathR)
    , "already exists"
  )
  expect_error(
    Binox(groupsA = getAGraph()$ga, pathOut = filepathO, randNetPath = filepathR)
    , "already exists"
  )
  expect_error(
    Binox(groupsA = getAGraph()$ga, groupsB = getAGraph()$gb, pathGroupsB = filepathB, randNetPath = filepathR)
    , "already exists"
  )
  expect_error(
    Binox(clusterFun = function() NULL,
          pathClusterGroupsA = filepathC,
          randNetPath = filepathR)
    , "already exists"
  )

  # type checks on groupsA en groupsB
  expect_error(
    Binox(groupsA = "foo", randNetPath = filepathR)
    , "groupsA must be either a list of character vectors or a data frame"
  )
  expect_error(
    Binox(groupsA = getAGraph()$ga, groupsB = "foo", randNetPath = filepathR)
    , "groupsB must be either a list of character vectors or a data frame"
  )

  # no support for clusters vs clusters
  expect_error(
    Binox(groupsA = getAGraph()$ga,
          randNetPath = filepathR,
          clusterFun = function() NULL)
    , "both groups A and groups B have to be specified"
  )

})

# ---------------------------------------------------------------------------- #
# Binox version
# ---------------------------------------------------------------------------- #

test_that("Binox version reports the version", {
  expect_match(Binox_version(), "version:")
})

# ---------------------------------------------------------------------------- #
# Binox randomize
# ---------------------------------------------------------------------------- #

test_that("Binox can generate random networks from igraphs and data frames", {
  filepath <- tempfile()
  expect_false(file.exists(filepath))
  expect_true(Binox_randomizeNetwork(randNetPath = filepath, network = getAGraph()$ig))
  expect_true(file.exists(filepath))
  expect_match(readLines(filepath)[1], "BinoX-Run")

  filepath <- tempfile()
  expect_false(file.exists(filepath))
  expect_true(Binox_randomizeNetwork(randNetPath = filepath,
                                     network = getAGraph()$ig,
                                     edgeWeightName = "w"))
  expect_true(file.exists(filepath))
  expect_match(readLines(filepath)[1], "BinoX-Run")

  filepath <- tempfile()
  expect_false(file.exists(filepath))
  expect_true(Binox_randomizeNetwork(randNetPath = filepath, network = getAGraph()$df))
  expect_true(file.exists(filepath))
  expect_match(readLines(filepath)[1], "BinoX-Run")

  filepath <- tempfile()
  expect_false(file.exists(filepath))
  expect_true(Binox_randomizeNetwork(randNetPath = filepath,
                                     network = getAGraph()$df,
                                     edgeWeightName = "w"))
  expect_true(file.exists(filepath))
  expect_match(readLines(filepath)[1], "BinoX-Run")

  # Non existing edge should give error
  filepath <- tempfile()
  expect_false(file.exists(filepath))
  expect_error(Binox_randomizeNetwork(randNetPath = filepath,
                                      network = getAGraph()$ig,
                                      edgeWeightName = "muhaha"))

  filepath <- tempfile()
  expect_false(file.exists(filepath))
  expect_error(Binox_randomizeNetwork(randNetPath = filepath,
                                      network = getAGraph()$df,
                                      edgeWeightName = "muhaha"))

  # No existing edge attribute, should give error
  filepath <- tempfile()
  expect_false(file.exists(filepath))
  expect_error(Binox_randomizeNetwork(randNetPath = filepath,
                                      network = delete_edge_attr(getAGraph()$ig, "w")),
               "Edge weight column is missing")

  filepath <- tempfile()
  expect_false(file.exists(filepath))
  expect_error(Binox_randomizeNetwork(randNetPath = filepath,
                                      network = getAGraph()$df[1:2]),
               "Edge weight column is missing")

  # Explicitly saying there is no edge attribute works, then all edges have weight 1
  filepath <- tempfile()
  expect_false(file.exists(filepath))
  expect_true(Binox_randomizeNetwork(randNetPath = filepath,
                                     network = getAGraph()$ig,
                                     edgeWeightName = NA))

  filepath <- tempfile()
  expect_false(file.exists(filepath))
  expect_true(Binox_randomizeNetwork(randNetPath = filepath,
                                     network = getAGraph()$df,
                                     edgeWeightName = NA))
})

test_that("Binox randomize can read/write (unrandomized) network file", {
  i_filepath <- tempfile(); expect_false(file.exists(i_filepath))
  r_filepath <- tempfile(); expect_false(file.exists(r_filepath))
  # can write
  expect_true(Binox_randomizeNetwork(randNetPath = r_filepath,
                                     networkPath = i_filepath,
                                     network = getAGraph()$ig))
  expect_true(file.exists(i_filepath))
  expect_true(file.exists(r_filepath))
  # can read
  r_filepath <- tempfile(); expect_false(file.exists(r_filepath))
  expect_true(Binox_randomizeNetwork(randNetPath = r_filepath,
                                     networkPath = i_filepath))
})

test_that("Binox randomize can handle extra parameters", {
  filepath <- tempfile()
  expect_true(Binox_randomizeNetwork(randNetPath = filepath,
                                     network = getAGraph()$df,
                                     seed = 1234,
                                     cutoff = 0.6))
})

test_that("Binox can randomize verbosely", {
  if ("Binox_silent" %in% skipList) skip("I don't like noisy unit tests")

  filepath <- tempfile()
  expect_output(
    Binox_randomizeNetwork(randNetPath = filepath, network = getAGraph()$df, verbose = T),
    "BinoX")
})

# ---------------------------------------------------------------------------- #
# Binox enrich
# ---------------------------------------------------------------------------- #

test_that("Binox enrich can work with both gene lists and df's as groups input", {
  # Prepare a workflow for Binox
  g <- getAGraph(100)
  la <-  length(unique(g$ga[[2]]))
  lb <-  length(unique(g$gb[[2]]))
  filepathR <- tempfile()
  filepathN <- tempfile()
  Binox_randomizeNetwork(network = g$ig, networkPath = filepathN, randNetPath = filepathR)
  expect_true(file.exists(filepathN))
  expect_true(file.exists(filepathR))

  B1 <- Binox(groupsA = g$ga, randNetPath = filepathR)
  expect_equal(ncol(B1), 8)
  expect_equal(nrow(B1), la*la)

  B2 <- Binox(groupsA = g$ga, randNetPath = filepathR, largeOutput = F)
  expect_equal(B1, B2)

  B3 <- Binox(groupsA = g$ga, randNetPath = filepathR, largeOutput = T)
  expect_equal(ncol(B3), 17)
  expect_equal(nrow(B3), la*la)

  ga_ls <- lapply(by(g$ga, g$ga[[2]], function(gs) gs[[1]]), identity)
  B4 <- Binox(groupsA = ga_ls, randNetPath = filepathR)
  expect_equal(ncol(B4), 8)
  expect_equal(nrow(B4), la*la)

  B5 <- Binox(groupsA = ga_ls, randNetPath = filepathR, largeOutput = T)
  expect_equal(ncol(B5), 17)
  expect_equal(nrow(B5), la*la)
})

test_that("Binox enrich can read/save files", {
  # Prepare a workflow for Binox

  filepathA <- tempfile()
  filepathB <- tempfile()
  filepathN <- tempfile()
  filepathR <- tempfile()
  filepathO <- tempfile()

  g <- getAGraph(100)
  la <-  length(unique(g$ga[[2]]))
  lb <-  length(unique(g$gb[[2]]))
  Binox_randomizeNetwork(network = g$ig, networkPath = filepathN, randNetPath = filepathR)
  expect_true(file.exists(filepathN))
  expect_true(file.exists(filepathR))
  expect_false(file.exists(filepathA))
  expect_false(file.exists(filepathB))
  expect_false(file.exists(filepathO))

  # can write groups?
  B1 <- Binox(groupsA = g$ga, pathGroupsA = filepathA,
              groupsB = g$gb, pathGroupsB = filepathB,
              randNetPath = filepathR)
  expect_equal(ncol(B1), 8)
  expect_equal(nrow(B1), la*lb)
  expect_true(file.exists(filepathA))
  expect_true(file.exists(filepathB))

  # can read groups?
  B2 <- Binox(pathGroupsA = filepathA,
              pathGroupsB = filepathB,
              randNetPath = filepathR)
  expect_equal(B1, B2)

  # can write Binox output?
  B3 <- Binox(pathGroupsA = filepathA,
              pathGroupsB = filepathB,
              pathOut     = filepathO,
              randNetPath = filepathR)
  expect_equal(B1, B3)
  expect_true(file.exists(filepathO))
  o <- readr::read_tsv(filepathO, col_types = "iccddcdc")
  expect_equal(B1$FDR, o$FDR)
})

test_that("Binox enrich handles too small inputs correctly", {
  # Prepare a workflow for Binox
  g <- getAGraph()
  la <-  length(unique(g$ga[[2]]))
  lb <-  length(unique(g$gb[[2]]))
  filepathR <- tempfile()
  filepathN <- tempfile()
  Binox_randomizeNetwork(network = g$ig, networkPath = filepathN, randNetPath = filepathR)
  expect_true(file.exists(filepathN))
  expect_true(file.exists(filepathR))

  B1 <- Binox(groupsA = g$ga[1,], randNetPath = filepathR)
  expect_true(is.na(unique(B1$FDR)))

})

test_that("Binox can enrich verbosely", {
  if ("Binox_silent" %in% skipList) skip("I don't like noisy unit tests")

  # Prepare a workflow for Binox
  g <- getAGraph(100)
  la <-  length(unique(g$ga[[2]]))
  lb <-  length(unique(g$gb[[2]]))
  filepathR <- tempfile()
  filepathN <- tempfile()
  Binox_randomizeNetwork(network = g$ig, networkPath = filepathN, randNetPath = filepathR)
  expect_true(file.exists(filepathN))
  expect_true(file.exists(filepathR))

  expect_output(
    Binox(groupsA = g$ga, randNetPath = filepathR, verbose = T),
    "BinoX"
  )
})

# ---------------------------------------------------------------------------- #
# Binox enrich, with clustering
# ---------------------------------------------------------------------------- #

test_that("Binox can enrich with MGclus in between", {
  if ("Binox_silent" %in% skipList) skip("I don't like noisy unit tests")

  # Prepare a workflow for Binox
  g <- getAGraph(200, 1/50)
  cf_mgc <- prepare.MGclus.ClusterFun(g$ig, "w")
  filepathR <- tempfile()
  filepathN <- tempfile()
  Binox_randomizeNetwork(network = g$ig, networkPath = filepathN, randNetPath = filepathR)
  expect_true(file.exists(filepathN))
  expect_true(file.exists(filepathR))

  B1 <- Binox(groupsA = g$ga, groupsB = g$gb, randNetPath = filepathR,
              clusterFun = cf_mgc, largeOutput = T)
  expect_equal(ncol(B1), 17)

})

test_that("Binox can enrich with MCL in between", {
  if ("Binox_silent" %in% skipList) skip("I don't like noisy unit tests")

  # Prepare a workflow for Binox
  g <- getAGraph(200, 1/100)
  cf_mcl <- prepare.MCL.ClusterFun(g$ig, "w", gq = 0)
  filepathR <- tempfile()
  filepathN <- tempfile()
  Binox_randomizeNetwork(network = g$ig, networkPath = filepathN, randNetPath = filepathR)
  expect_true(file.exists(filepathN))
  expect_true(file.exists(filepathR))

  B1 <- Binox(groupsA = g$ga, groupsB = g$gb, randNetPath = filepathR,
              clusterFun = cf_mcl, largeOutput = T)
  expect_equal(ncol(B1), 17)

})
