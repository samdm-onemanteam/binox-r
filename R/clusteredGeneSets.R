# ............................................................................ #
# f:getClusteredGeneSets

#' Cluster gene sets from datasets/experiments
#'
#' Cluster gene sets for specific datasets/experiments with the specified type of
#' gene identifiers.
#'
#' @export
getClusteredGeneSets <- function(object, dataEntries = getDataEntries(object),
                                 recompute = FALSE, ncores = 1) {

  annotationDbNames <- getAnnotationDbNames(object, dataEntries)
  ensureMappings(object, annotationDbNames)

  task <- function(dataEntry, experimentName) {

    path <- getPath.ClusteredGeneSet(object, experimentName)

    loadResult(object, path, recompute,
               function() loadClusteredGeneSet.compute(object, path, dataEntry, experimentName),
               function() loadClusteredGeneSet.restore(path))

  }

  return(runTasks(task, dataEntries, ncores))
}

# ---------------------------------------------------------------------------- #
# Private functions
# ---------------------------------------------------------------------------- #

notImplementedYet <- function(dataEntry, geneSet) {
    stop("No implementation yet for default clustering method")
}

# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# loading/computing

loadClusteredGeneSet.restore <- function(path) {
  # sometimes genelists are empty, which causes readr to crash the session.
  # see https://github.com/hadley/readr/issues/378
  if (file.info(path)$size > 0) {
    clusterDf <- readr::read_tsv(path, col_types = 'cc')
    # `by` to split the data frame per module
    # `lapply( ... , identity)` to transform a `by` object ot a `list`
    # `[unique(clusterDf$module)]` to sort the modules in the list in the same
    # order as in the data frame.
    modules <- lapply(by(clusterDf, list(clusterDf$module), function(module) {
      module[,2]
    }), identity)[unique(clusterDf$module)]
    return(modules)
  } else {
    return(list(character()))
  }
}

loadClusteredGeneSet.compute <- function(object, path, dataEntry, experimentName) {

  geneSet <- getGeneSets(object, namedlist(experimentName, dataEntry),
                         recompute = F)[[1]]

  clusters <- object@geneSetClusterMethod@fun(dataEntry, geneSet)
  if (is.null(names(clusters))) {
    names(clusters) <- paste0("m_", 1:length(clusters))
  }

  # Write to disk
  clusterDf <- foreach(cluster = clusters, id = names(clusters),
                       .combine = "rbind") %do% {
    cbind(id, cluster)
  }
  colnames(clusterDf) <- c("module", "gene")
  readr::write_tsv(as.data.frame(clusterDf), path)

  return(clusters)
}
