# ............................................................................ #
# f:getMappings

#' Compute/load platform specific probeset mappings
#'
#' @inheritParams getDataEntries
#' @inheritParams setDataEntries
#' @inheritParams getDatasets
#' @param annotationDbNames the names of the annotation databases which you wish
#'   to map to another type of identifier. Possible values are
#'   \code{"hgu133a.db"} and \code{"hgu133plus2.db"} (when using the default
#'   setting for \code{idMapper}).
#' @param idType the kind of gene identifiers you which to map to. Possible
#'   values are 'ENTREZID' and 'UNIPROT' (when using the default setting
#'   for \code{idMapper}).
#' @param idMapper The function you wish to use for mapping platform specific
#'   probesets to gene identifiers. By default the \code{\link{getFirstMatch}}
#'   function is used.
#'
#' @export
getMappings <- function(object, annotationDbNames = getAnnotationDbNames(object),
                        recompute = FALSE) {
  mappingWorker(object, annotationDbNames, recompute, loadInMemory = TRUE)
}

#' Get platform specific probesets mapped to gene identifiers
#'
#' The \code{AnnotationDbi} package is used to map probesets to gene
#' identifiers, when one probeset matches multiple gene identifiers, the first
#' one is picked.
#'
#' @param annotationDbName the name of the annotation database which you wish to
#'   map to another type of identifier. Possible values are \code{"hgu133a.db"}
#'   and \code{"hgu133plus2.db"}.
#' @param idType The kind of gene identifiers you which to map to. Possible
#'   values are 'ENTREZID' and 'UNIPROT' among others. See the \code{hgu133a.db}
#'   and \code{hgua33plus2.db} packages on Bioconductor to get a full list of
#'   supported gene identifiers.
#'
#' @export
getFirstMatch <- function(annotationDbName, idType) {

  mapIds <- keys <- NULL # This is to make RCheck shut up

  if (grepl("_", annotationDbName)) {
    split <- strsplit(annotationDbName, "_")[[1]]
    DBpackage        <- split[1]
    probesets_idType <- split[2]
  } else {
    DBpackage <- annotationDbName
    probesets_idType <- "PROBEID"
  }

  # Load annotation database
  library(DBpackage, character.only = T)

  mappingVect <- mapIds(
    get(DBpackage),
    keys = keys(get(DBpackage), probesets_idType),
    keytype = probesets_idType,
    column = idType
  )
  mapping <- data.frame(PROBEID = names(mappingVect),
                        MAPPING = mappingVect,
                        stringsAsFactors = F,
                        row.names = 1:length(mappingVect))
}

# ---------------------------------------------------------------------------- #
# Private functions
# ---------------------------------------------------------------------------- #

# Check if mapping file exists and generate it if doesn't
ensureMappings <- function(object, annotationDbNames) {
  mappingWorker(object, annotationDbNames,
                recompute = FALSE, loadInMemory = FALSE)
}

mappingWorker <- function(object, annotationDbNames, recompute, loadInMemory) {

  # We don't permute mappings
  if (object@permute) {
    object@permute <- FALSE
  }

  annotationDbName <- NULL # This is to make RCheck shut up
  mappings <- foreach(annotationDbName = annotationDbNames) %do% {
    path <- getPath.Mapping(object, annotationDbName)
    loadResult(object, path, recompute,
               function() loadMapping.compute(object, path, annotationDbName),
               ifelse(loadInMemory, function() loadMapping.restore(path)
                                  , NA))
  }
  names(mappings) <- annotationDbNames

  if (loadInMemory) return(mappings)
  else              return(NULL)
}

# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# loading/computing

loadMapping.restore <- function(path) {
  readr::read_tsv(path, col_types = "cc")
}

loadMapping.compute <- function(object, path, annotationDbName) {
  mapping <- object@idMapper@fun(annotationDbName, object@idType)
  readr::write_tsv(mapping, path)
  return(mapping)
}
