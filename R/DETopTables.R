# ............................................................................ #
# f:getDETopTables

#' Get toptables for a specific experiment/dataset.
#'
#' @inheritParams getDataEntries
#' @inheritParams setDataEntries
#' @inheritParams DataEntry
#' @inheritParams getDatasets
#' @param DETopTableFun The function to use for generating a toptable of
#'   differential experession statistics from a dataset (A
#'   \code{BioBase::ExpressionSet} object). By default the
#'   \code{\link{makeModeratedTTestTopTable}} function is used.
#'
#' @export
getDETopTables <- function(object, dataEntries = getDataEntries(object),
                           recompute = FALSE, ncores = 0) {

  task <- function(dataEntry, experimentName) {

    path <- getPath.DETopTable(object, experimentName)
    loadResult(object, path, recompute,
               function() loadDETopTable.compute(object, path, dataEntry, experimentName),
               function() loadDETopTable.restore(path))
  }

  return(runTasks(task, dataEntries, ncores))
}

# ............................................................................ #
# f:makeModeratedTTestTopTable

#' Make a toptable of differential expression statistics
#'
#' @param eset A \code{BioBase::ExpressionSet} object
#' @inheritParams dataEntryToJSON
#'
#' @export
makeModeratedTTestTopTable <- function(eset, dataEntry) {

  if (dataEntry@isLog2) {
    expressionMatrix <- exprs(eset)
  } else {
    expressionMatrix <- log2(exprs(eset))
  }

  # Do moderated T test. If no block design is given, then mimimizing the sum of
  # squares (which is the objective funtion of the linear model) will basically
  # just calculate the means of groups 't' and 'c'.
  if (length(dataEntry@block)) {
    designMatrix <- model.matrix(~ 0 + dataEntry@phenotypes + dataEntry@block)
  } else {
    designMatrix <- model.matrix(~ 0 + dataEntry@phenotypes)
  }
  # fit linear model to each probeset. lmFit is extemely picky about what names
  # are allowed for the design matrix, no special symbols are allowed.
  colnames(designMatrix)[1:2] <- levels(dataEntry@phenotypes)
  colnames(designMatrix) <- gsub("dataEntry@", "", colnames(designMatrix), fixed=T)
  fit <- lmFit(expressionMatrix, designMatrix)
  # define contrast of interest: difference between mean for groups 't' and 'c'
  contrastMatrix <- makeContrasts(contrasts = "t-c", levels = designMatrix)
  fit <- contrasts.fit(fit, contrastMatrix)
  # shrink variance toward common value using emperical Bayes
  fit <- eBayes(fit)
  # return a data frame sorted on p values of difference in means vs 0 (= first
  # coefficient), optionally with correction for blocked design. topTable also
  # does multiple testing correction, the default method is FDR correction with
  # the Benjamini Hochberg method.
  DETopTable <- topTable(fit, coef = 1, number = nrow(expressionMatrix))
  DETopTable <- cbind(PROBEID = rownames(DETopTable), DETopTable)
  rownames(DETopTable) <- 1:nrow(DETopTable)
  DETopTable$PROBEID <- as.character(DETopTable$PROBEID)

  return(DETopTable)
}

# ---------------------------------------------------------------------------- #
# Private functions
# ---------------------------------------------------------------------------- #

# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# loading/computing

loadDETopTable.restore <- function(path) {
  DETopTable <- readr::read_tsv(path)
  DETopTable$PROBEID <- as.character(DETopTable$PROBEID)
  return(DETopTable)
}

loadDETopTable.compute <- function(object, path, dataEntry, experimentName) {

  dataset <- getDatasets(object, namedlist(experimentName, dataEntry),
                         recompute =  FALSE)[[1]]

  DETopTable <- object@DETopTableFun@fun(dataset, dataEntry)

  readr::write_tsv(DETopTable, path)

  return(DETopTable)
}
