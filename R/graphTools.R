# ............................................................................ #
# f:create.randomizeGeneSet

#' Creates a closure that randomizes gene sets according to a gene graph.
#'
#' Can be used for making randomisations of gene sets when expression matrices
#' are not available. The closure will replace nodes in a gene set with
#' new nodes that have similar node degree.
#'
#' @param graph An igraph object
#' @param error Allowed deviation in node degree
#' @param edgeWeightName Which edge attribute represents edge weight? Can be
#'   ommitted.
#' @param edgeWeightCutoff A cutoff that will be used to trim the network
#'   before using it.
#'
#' @examples
#' \dontrun{
#'   bm <- PWABenchmark(...)
#'   g <- igraph::sample_pa(100)
#'   randomize_gene_set <- create.randomizeGeneSet(g)
#'
#'   permuted_gene_sets <- getPermutations(
#'     bm, permutations = 1:10,
#'     what = randomize_gene_set
#'  )
#' }
#'
#' @export
create.randomizeGeneSet <- function(graph, error = 0.05,
                                    edgeWeightName, edgeWeightCutoff) {
  graph_df <- as_data_frame(graph)
  # If no edge weight is given, set all edge weights to 1
  if (missing(edgeWeightName)) {
    graph_df$edgeW   <- 1
  } else {
    graph_df$edgeW <- graph_df[[edgeWeightName]]
  }
  # If no edge weight cutoff is given, then set the cutoff 10% lower than the
  # lowest edge weight, effectively preventing a cutoff.
  if (missing(edgeWeightCutoff)) {
    edgeWeightCutoff = abs(min(graph_df$edgeW)) * -1.1
  }
  node_degrees <- getNodeDegrees(graph_df[[1]], graph_df[[2]], graph_df$edgeW,
                                 edgeWeightCutoff)

  function(object, dataEntries = getDataEntries(object),
           recompute = F, ncores = 1) {

    objectPerm <- object
    objectPerm@permute <- T

    task <- function(dataEntry, experimentName) {

      path_perm <- getPath.GeneSet(objectPerm, experimentName)
      orig_experimentName <- gsub("_p[[:digit:]]+$", "", experimentName)

      loadResult(object, path_perm, recompute,
                 function() loadRandGeneSet.compute(object, path_perm,
                                                    orig_experimentName,
                                                    node_degrees, error),
                 function() loadGeneSet.restore(path_perm))

    }

    return(runTasks(task, dataEntries, ncores))
  }
}

# ---------------------------------------------------------------------------- #
# Private functions
# ---------------------------------------------------------------------------- #

# Replaces nodes in node_set by other nodes with similar node degree. This
# method is non-deterministic and creates not-highly overlapping random sets,
# even if the intput sets are similar
# Input:
#   `node_set`     : the nodes which you want to randomize
#   `node_degrees` : an object created by `get_node_degrees` function
# Output:
#   A new list with every node randomized by `randomize_node` function
randomize_node_set <- function(node_set, node_degrees, error = 0.05) {
  out_nodes <- rep(NA, length(node_set))
  for (i in 1:length(node_set)) {
    node = node_set[i]
    if (node %in% node_degrees$node) {
      # get nodes with similar degree up to fraction `error`
      closest <- get_closest_degree_set(node, node_degrees, error)
      # remove nodes that have been picked before and nodes from input list
      closest_new <- closest[which(!(closest %in% c(out_nodes, node_set)))]
      if (length(closest_new) != 0) {
        out_nodes[i] <- sample(closest_new, 1)
      }
    }
    if (is.na(out_nodes[i])) {
      # If we don't know the node degree, then pick a completeley random node
      # that is not in the input and has not been picked before
      out_nodes[i] <- sample(
        node_degrees$node[which(!node_degrees$node %in% c(out_nodes, node_set))],
        1
      )
    }
  }
  return(out_nodes)
}

# Get list of nodes whose degrees are within a certain error margin of the query
# node
get_closest_degree_set <- function(node, node_degrees, error = 0.05,
                                    external_node_degrees) {
  if (missing(external_node_degrees)) {
    node_degree <- node_degrees$degree[node_degrees$node == node]
  } else {
    node_degree <- external_node_degrees$degree[external_node_degrees$node == node]
  }
  node_degrees$node[
    node_degrees$degree <= node_degree * (1 + error) &
    node_degrees$degree >= node_degree * (1 - error)
  ]
}

# Make a node degree table from a graph using an edge-weight cutoff
getNodeDegrees <- function(nodesA, nodesB, edgeWeight, edgeWeightCutoff) {
  nodesA <- nodesA[edgeWeight > edgeWeightCutoff]
  nodesB <- nodesB[edgeWeight > edgeWeightCutoff]
  node_degrees_c <- sort(table(c(nodesA, nodesB)))
  node_degrees <- data.frame(
    node   = rownames(node_degrees_c),
    degree = unname(node_degrees_c),
    stringsAsFactors = F
  )
}

# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# loading/computing

loadRandGeneSet.compute <- function(object, path_perm, experimentName,
                                    node_degrees, error) {
  objectNoPerm <- object
  objectNoPerm@permute <- F
  path_orig <- getPath.GeneSet(objectNoPerm, experimentName)
  geneSet_orig <- loadGeneSet.restore(path_orig)
  geneSet_rand <- randomize_node_set(geneSet_orig, node_degrees, error)

  readr::write_tsv(data.frame(geneSet_rand), path_perm, col_names = FALSE)

  return(geneSet_rand)
}
