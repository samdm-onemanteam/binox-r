---
title: 'Demo: benchmarking pathway analysis'
author: "Sam De Meyer"
date: "June 8, 2016"
output: 
  html_document: 
    toc: yes
bibliography: library.bib
---

---
nocite: | 
  @Tarca2013
...

Introduction
------------

### Setup

First load the package and create a benchmark object:

```{r, warning=FALSE, results='hide'}
library(BinoX)

# `dataDir` should be a string corresponding to a directory, eg "~/dataDir"
dataDir <- "dataDir"
benchmark <- PWABenchmark(dataDir)
addDefaultDataEntries(benchmark)
```

The last line adds the default benchmarking data to the benchmark.

There is one more thing we need to do before we can run the benchmark. Every 
dataset in the benchmark has an associated KEGG pathway [@Kanehisa2000] that is
known to be enriched. So now we need to get the latest
[KEGG](http://www.genome.jp/kegg/) pathways. The 
[KEGGREST](https://bioconductor.org/packages/release/bioc/html/KEGGREST.html) 
library makes accessing KEGG a bit easier but getting all pathways in Entrez ids
still requires quite a lot of parsing:

```{r, eval=FALSE}
library(KEGGREST)
# Download KEGG pathways
pathw <- keggLink("pathway", "hsa")
pathw_df <- data.frame(KEGGid  = gsub("^[^:]+:", "", names(pathw)),
                       pathway = pathw)
# Translate KEGG ids to NCBI ids
conv <- keggConv("hsa", "ncbi-geneid")
conv_df <- data.frame(KEGGid = gsub("^[^:]+:", "", conv),
                      NCBIid = gsub("^[^:]+:", "", names(conv)))
# Now mold it into a named list of pathways
merged <- merge(pathw_df, conv_df)
targetGeneSets <- lapply(by(merged$NCBIid, merged$pathway, function(pathway) {
  as.character(pathway)
}), identity)
```

### True positives test

In this demo we will benchmark the Fisher exact test, the Ease test 
[@Hosack2003] and the Padog tool [@Tarca2012]. The first step is to generate the
input data for these tools from the benchmarking data.

```{r, eval=FALSE}
queryGeneSets <- getGeneSets(benchmark)
queryDatasets <- getDatasets(benchmark)
```

This will probably take a while (about 10 minutes in my case), because all the
datasets have to be downloaded from [NCBI
GEO](http://www.ncbi.nlm.nih.gov/geo/). It will only take this long the first
time. The datasets and results of intermediary calculations are saved in the
data directory (`dataDir`). So if you add tools later, the package will not have
to redo everything again.

If you have multiple processor cores, you can speed up the process a little by 
setting the `ncores` argument (optimal is your number of cpu's minus one, see 
*about multithreading* in the docs). This allows calculating gene sets from one
dataset while another dataset is still being dowloaded. If you which, you can
safely interrupt the process with `CTRL-C` (it migh take a minute before the
process stops). Everything that has been done so far will be saved, and if you
run the function agian it will pick up from where it stopped last time.

```{r, eval=FALSE}
queryGeneSets <- getGeneSets(benchmark, ncores = 3)
```

Then setup the tools you want to run:

```{r, eval=FALSE}
TPsetup <- list(
  "Fisher" = prepTool(fun     = getFisherTool(23e3),
                      queries = queryGeneSets,
                      targets = targetGeneSets,
                      prefix  = "demo"),
  "Ease"   = prepTool(fun     = getEaseTool(23e3),
                      queries = queryGeneSets,
                      targets = targetGeneSets,
                      prefix  = "demo"),
  "Padog"  = prepTool(fun     = getPadogTool(benchmark),
                      queries = queryDatasets,
                      targets = targetGeneSets,
                      prefix  = "demo")
)
```

The names of the tools in the list can be chosen arbitrarely. The `prepTool()`
function prepares a tool to be run in the benchmark, it takes four arguments:

1. The first is a function to actually run the tool. For example 
   `getFisherPlugin(2e23)` returns a function which does the fisher test with a 
   background of 23.000 genes. 
2. The second argument is the input for the tool. `queryGeneSets` is a list 
   containing sets of differentially expressed genes, one for each dataset from
   the benchmarking data. A set of differentially expressed genes is the input
   required for the Fisher test and the Ease test. The Padog tool works on gene
   expression matrices directly, so we give it `queryDatasets`, a list of gene
   expression matrices, instead of `queryGeneSets`.
3. The `targets` argument is a list of gene sets for which we want to calculate
   enrichment. In this example, `targetGeneSets` is a list of KEGG pathways.
4. The `prefix` option is some name to use for saving
   the output of the tools, e.g. the output file names for the fisher test will
   start with `demo.Fisher.` followed by the name of the test dataset.

This is quite a lot of code for setting up the tools, but it also allows for a 
lot of flexibility. Different tools have different requirements in terms of
input and setup. What is presented here is a uniform interface for running different
enrichment tool.

Now we are ready to run the benchmark on the test data:

```{r, eval=FALSE}
runTPBenchmark(benchmark, TPsetup)

TPresults <- summarizeTPResults(benchmark, "demo")
TPplot(TPresults, "p", breaks = 500)
TPplot(TPresults, "r")
```

```{r, eval=c(2:13), message=FALSE, echo=FALSE, fig.width=10}
TPresults <- summarizeTPResults(benchmark, "demo", toolNames = names(TPsetup), saveAs = "demo_1")
TPresults <- loadTPSummary(benchmark, "demo_1")

library(cowplot)
theme_set(theme_gray())

ggdraw() + #                                      x    y  w    h
  draw_plot(TPplot(TPresults, "p", breaks = 500)
            + guides(colour = guide_legend(title = "Tool"),
                     linetype = guide_legend(title = "Prefix")),
            0  , 0, 1/2, 1.0) +
  draw_plot(TPplot(TPresults, "r"),
            1/2, 0, 1/2, 1.0)
```

The warning happens because for five target pathways the p-value was 1. Since
every pathway that does not overlap with the gene set has a p-value of 1 (or
an undefined p-value for EASE) there are ties is the rank. Since a p-value of one
means *definately not enriched*, we can say that the tool failed to prioritise
the target pathway, and we set the rank to 100%.

### False positive test

To estimate the false positive ratio, we shuffle the phenotype labels of the 
benchmarkding data a number of times, and then run the whole analysis on this 
permuted data. Because shuffled data gives, in most cases, no differentially 
expressed genes, we take the top `n` genes where `n` is drawn from the sizes of 
the real gene sets. The first step is to make a function that takes the top `n` 
genes and tell the benchmark object to use that for getting gene sets. The
second step is to actually permute the input data. If we want 10 permutation of
each input dataset we can do:

```{r, eval=FALSE}
sizes <- sapply(queryGeneSets, length)
perm_geneset_method <- function(toptable) {
  rowstotake <- sample(sizes, 1)
  toptable[1:rowstotake, "MAPPING"]
}
settings(benchmark) <- list(
  geneSetMethod = plug("labelswap_top_n", perm_geneset_method)
)

pQueryGeneSets <- getPermutations(benchmark, permutations = 1:10, what = "GeneSets")
pQueryDatasets <- getPermutations(benchmark, permutations = 1:10, what = "Datasets")
```

Then set up the tools to run on the permutations:

```{r, eval=FALSE}
FPsetup <- list(
  "Fisher" = prepTool(fun     = getFisherTool(23e3),
                      queries = pQueryGeneSets,
                      targets = targetGeneSets,
                      prefix  = "demo"),
  "Ease"   = prepTool(fun     = getEaseTool(23e3),
                      queries = pQueryGeneSets,
                      targets = targetGeneSets,
                      prefix  = "demo"),
  "Padog"  = prepTool(fun     = getPadogTool(benchmark),
                      queries = pQueryDatasets,
                      targets = targetGeneSets,
                      prefix  = "demo")
)
```

And run the false postive benchmark:

```{r, eval=FALSE}
runFPBenchmark(benchmark, FPsetup)

FPresults <- summarizeFPResults(benchmark, "demo")
FPplot(FPresults, breaks = 0.01)
FPplot(FPresults, breaks = 100)
```

```{r, eval=c(2:11), message=FALSE, echo=FALSE, warning=FALSE, fig.width=10}
FPresults <- summarizeFPResults(benchmark, "demo", toolNames = names(FPsetup), saveAs = "demo_2")
FPresults <- loadFPSummary(benchmark, "demo_2")

ggdraw() +
  draw_plot(FPplot(FPresults, breaks = c(0.01))
            + theme(legend.position = "none"),
            0  , 0, .45, 1.0) +
  draw_plot(FPplot(FPresults, breaks = 100) 
            + guides(colour = guide_legend(title = "Tool"),
                     linetype = guide_legend(title = "Prefix")),
            .45, 0, .55, 1.0)
```

Advanced usage
--------------

### The data directory

If you now take a look at the `dataDir`, you will see that a lot of files have 
been generated. The `GEO` datasets have been saved in `dataDir/GEOs`. The output
of the tools is saved in `dataDir/Results` and a summary table of the 
performance in `dataDir/Summary`. The output of the false positive tests is in 
`dataDir/Permutations`, which has the same substructe as it's parent directory. 
For example, the output of the tools is now saved in 
`dataDir/Permutations/Results`. You can safely remove any of the files (except
for the ones in `dataDir/DataEntries`), they will be automatically recomputed
only when they are needed.

### Changing benchmark settings

Almost all the default behaviour in the benchmark can be changed. If you wish to
use `Ensembl` ids [@Cunningham2014] for example:

```{r, eval = FALSE}
settings(benchmark) <- list(idType = "ENSEMBL")
```

Now our KEGG pathways are still in Entrez ids, so we will have to convert them
to Ensembl ids as well:

```{r, message=FALSE, eval=FALSE}
library(AnnotationDbi)
EntrezToEnsembl <- as.list(org.Hs.eg.db::org.Hs.egENSEMBL)
targetGeneSets_Ensembl <- lapply(targetGeneSets, function(geneSet) {
  geneSet_Ensembl <- unname(sapply(geneSet, function(gene) {
    EntrezToEnsembl[[gene]][1]
  }))
  geneSet_Ensembl[!is.na(geneSet_Ensembl)]
})
```

Now run the same code as above again, using `targetGeneSets_Ensembl` instead of 
`targetGeneSets` and everything will be done using Ensembl ids instead of Entrez
ids.

It is also possible to change the computation of differential expression
statistics, or the method of selecting differentially expressed genes from test
statistics (and more). See the documention for details.

# References
